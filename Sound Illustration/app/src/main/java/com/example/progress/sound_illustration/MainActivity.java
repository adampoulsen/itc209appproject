package com.example.progress.sound_illustration;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends ActionBarActivity {
    private static final String DEBUG_TAG = "HttpExample";
    public ArrayList<Sound> sounds = new ArrayList<Sound>();
    ListView listview;
    Button btnDownload;
    JSONArray rows;
    ArrayList<Integer> randomInts;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listview = (ListView) findViewById(R.id.listview);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            btnDownload.setEnabled(true);
        } else {
            btnDownload.setEnabled(false);
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                String o = listview.getItemAtPosition(position).toString();
                TextView IDTextView = (TextView)arg1.findViewById(R.id.sound);
                String sound = IDTextView.getText().toString();
                Sound soundObj = new Sound(0, null, null, null, null, null);
                for (int i=0; i<sounds.size();i++){

                    if(sounds.get(i).getSound().equals(sound)) {
                        soundObj = sounds.get(i);
                    }
                }
                Intent myIntent = new Intent(getBaseContext(), DescriptionActivity.class);
                myIntent.putExtra("soundObj", soundObj);
                startActivity(myIntent);
            }
        });
    }

    public void downloadButtonClickHandler(View view) {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                processJson(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key=1eoNODCZSMtIvAh0hCHOgl_B6UNaXjoNoSZbHnnAEj2k");
        btnDownload.setText("Update list");
        progress = ProgressDialog.show(this, "", "Loading sounds", true);
    }

    public void submitNewButtonClickHandler(View view) {
        Intent myIntent = new Intent(getBaseContext(), SubmissionActivity.class);
        startActivity(myIntent);
    }

    public void helpButtonClickHandler(View view) {
        Intent myIntent = new Intent(getBaseContext(), HelpActivity.class);
        startActivity(myIntent);
    }


    public void guessGameButtonClickHandler(View view) {
        Intent myIntent = new Intent(getBaseContext(), GuessingGameActivity.class);
        ArrayList<Sound> randomSounds = new ArrayList<Sound>();
        randomInts = new ArrayList<Integer>();
        for (int i = 0; i < 3; i++) {
            randomSounds.add(randomSound());
        }
        myIntent.putExtra("soundObjArray", randomSounds);
        startActivity(myIntent);
    }

    private Sound randomSound() {
        Random ran = new Random();
        int min = 0;
        int max = sounds.size()-1;
        int r = ran.nextInt(max-min) + min;
        if (randomInts.contains(r)) {
            randomSound();
        }
        else {
            randomInts.add(r);
        }
        return sounds.get(r);
    }

    private void processJson(JSONObject object) {

        try {
            rows = object.getJSONArray("rows");

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");

                int ID = columns.getJSONObject(0).getInt("v");
                String sound = columns.getJSONObject(1).getString("v");
                String description = columns.getJSONObject(2).getString("v");
                String submissionMode = columns.getJSONObject(3).getString("v");
                String imageURL = columns.getJSONObject(4).getString("v");
                imageURL = imageURL.replace("https://drive.google.com/file/d/", "https://docs.google.com/uc?id=");
                imageURL = imageURL.replace("/view?usp=drivesdk", "");
                String grammaticalChoice = columns.getJSONObject(5).getString("v");
                Sound team = new Sound(ID, sound, description, submissionMode, imageURL, grammaticalChoice);
                sounds.add(team);
            }

            final SoundsAdapter adapter = new SoundsAdapter(this, R.layout.sound, sounds);
            listview.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        findViewById(R.id.guessGame).setVisibility(View.VISIBLE);
        findViewById(R.id.guessGame).setClickable(true);
        progress.dismiss();
    }
}




