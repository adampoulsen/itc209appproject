package com.example.progress.sound_illustration;

import java.io.Serializable;

/**
 * Created by kstanoev on 1/14/2015.
 * Modifed by Adam Poulsen
 */
public class Sound implements Serializable{
    private int ID;
    private String sound;
    private String description;
    private String submissionMode;
    private String image;
    private String grammaticalChoice;

    public Sound(int ID, String sound, String description, String submissionMode, String image, String grammaticalChoice)
    {
        this.setID(ID);
        this.setSound(sound);
        this.setDescription(description);
        this.setSubmissionMode(submissionMode);
        this.setImage(image);
        this.setGrammaticalChoice(grammaticalChoice);
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubmissionMode() {
        return submissionMode;
    }

    public void setSubmissionMode(String submissionMode) {
        this.submissionMode = submissionMode;
    }

    public String getImage() { return image; };

    public void setImage(String image) { this.image = image; }

    public String getGrammaticalChoice() { return grammaticalChoice; };

    public void setGrammaticalChoice(String grammaticalChoice) { this.grammaticalChoice = grammaticalChoice; }

}
