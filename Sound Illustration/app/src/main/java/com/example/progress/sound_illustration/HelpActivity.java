package com.example.progress.sound_illustration;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Adam on 12-Oct-16.
 */
public class HelpActivity extends MainActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        TextView helpTextView = (TextView)findViewById(R.id.helpText);
        helpTextView.setText("Sound Illustration is a thought experiment for the hypothesis: Sound can be experienced through kinamemory - the experience of having a conscious visionary fantasy " +
                "upon being given a vivid visual explanation.  This App is intended to make you question your concept of sound and invites you to contribute to the exploration of this hypothesis.  " +
                "A Sound and its description are presented in this format: [Sound] sounds like what [description] look(s) like\n" +
                "\n" +
                "Using the App:\n" +
                "       Searching - Search sounds by clicking the 'DOWNLOAD SOUNDS' button, scrolling through the loaded list, and clicking on a desired sound to get the description and image.\n" +
                "       Submitting - Submit your own sound and description by clicking the 'SUBMIT NEW' button, enter sound information, upload image, click the 'SUBMIT' button, and send an email as " +
                "instructed without changing the email format.\n" +
                "       Playing the guessing game - The guessing game poses a description and three possible sounds to choose from.  Play by clicking the 'GUESSING GAME' button and selecting the sound" +
                "which you think is correct\n" +
                "\n" +
                "Creator: Adam Poulsen\n" +
                "Email questions/opinions/complaints to adamrosspoulsen@hotmail.com");

    }

    public void backButtonClickHandler(View view) {
        finish();
    }
}
