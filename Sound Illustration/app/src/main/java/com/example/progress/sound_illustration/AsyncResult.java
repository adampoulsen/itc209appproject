package com.example.progress.sound_illustration;

import org.json.JSONObject;

/**
 * Created by kstanoev on 1/14/2015.
 * Modifed by Adam Poulsen
 */
interface AsyncResult
{
    void onResult(JSONObject object);
}