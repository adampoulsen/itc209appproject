package com.example.progress.sound_illustration;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

/**
 * Created by Adam on 09-Oct-16.
 */
public class DescriptionActivity extends MainActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description);
        Intent intent = getIntent();
        Sound soundObj = (Sound) intent.getSerializableExtra("soundObj");
        TextView soundTextView = (TextView)findViewById(R.id.soundData);
        soundTextView.setText(soundObj.getSound() + " sounds like what " + soundObj.getDescription() + " " + soundObj.getGrammaticalChoice() + " like");
        ImageView image = (ImageView)findViewById(R.id.image);
        try {
            image.setImageBitmap(new DownloadImageTask(image, getBaseContext()).execute(soundObj.getImage()).get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    public void backButtonClickHandler(View view) {
        finish();
    }

}
