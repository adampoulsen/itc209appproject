package com.example.progress.sound_illustration;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Adam on 11-Oct-16.
 */
public class GuessingGameActivity extends MainActivity{
    Sound soundObjCorrect;
    Button option1;
    Button option2;
    Button option3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guessing_game);
        Intent intent = getIntent();
        ArrayList<Sound> soundObjArray = (ArrayList<Sound>) intent.getSerializableExtra("soundObjArray");
        Random ran = new Random();
        int r = ran.nextInt(((soundObjArray.size()-1)-1) + 0);
        soundObjCorrect = soundObjArray.get(r);
        TextView soundTextView = (TextView)findViewById(R.id.soundData);
        soundTextView.setText("What sounds like what " + soundObjCorrect.getDescription() + " " + soundObjCorrect.getGrammaticalChoice() + " like?");
        option1 = (Button)findViewById(R.id.option1);
        option2 = (Button)findViewById(R.id.option2);
        option3 = (Button)findViewById(R.id.option3);
        option1.setText(soundObjArray.get(0).getSound());
        option2.setText(soundObjArray.get(1).getSound());
        option3.setText(soundObjArray.get(2).getSound());

    }

    public void option1ButtonClickHandler(View view) {
        checkAnswer(option1.getText().toString());
    }

    public void option2ButtonClickHandler(View view) {
        checkAnswer(option2.getText().toString());
    }

    public void option3ButtonClickHandler(View view) {
        checkAnswer(option3.getText().toString());
    }

    private void checkAnswer (String sound) {
        if (sound.equals(soundObjCorrect.getSound())) {
            Toast.makeText(getBaseContext(), "Correct", Toast.LENGTH_SHORT).show();
        }
        else {Toast.makeText(getBaseContext(), "Incorrect: Answer is " + soundObjCorrect.getSound(), Toast.LENGTH_SHORT).show();}
        finish();
    }

    public void backButtonClickHandler(View view) {
        finish();
    }
}
