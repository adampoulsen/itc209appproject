package com.example.progress.sound_illustration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by kstanoev on 1/14/2015.
 * Modifed by Adam Poulsen
 */
public class SoundsAdapter extends ArrayAdapter<Sound> {

    Context context;
    public ArrayList<Sound> sounds;

    public SoundsAdapter(Context context, int textViewResourceId, ArrayList<Sound> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.sounds = items;
    }

    @Override
    public View getView(int soundID, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.sound, null);
        }

        Sound o = sounds.get(soundID);
        if (o != null) {
            //TextView ID = (TextView) v.findViewById(R.id.ID);
            TextView sound = (TextView) v.findViewById(R.id.sound);
            //TextView description = (TextView) v.findViewById(R.id.description);
            TextView submissionMode = (TextView) v.findViewById(R.id.submissionMode);

            //ID.setText(String.valueOf(o.getID()));
            sound.setText(String.valueOf(o.getSound()));
            //description.setText(String.valueOf(o.getDescription()));
            submissionMode.setText(String.valueOf(o.getSubmissionMode()));


        }
        return v;
    }
}

