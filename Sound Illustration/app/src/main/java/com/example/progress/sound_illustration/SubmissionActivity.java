package com.example.progress.sound_illustration;

import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Adam on 09-Oct-16.
 */
public class SubmissionActivity extends MainActivity {
    Spinner dropdown;
    private static int RESULT_LOAD_IMAGE = 1;
    String picturePath;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.submission);
        dropdown = (Spinner)findViewById(R.id.grammaticalChoice);
        String[] items = new String[]{"look like", "looks like"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        dropdown.setAdapter(adapter);
    }

    public void submitButtonClickHandler(View view) {
        EditText sound = (EditText)findViewById(R.id.sound);
        EditText description = (EditText)findViewById(R.id.description);
        String grammaticalChoice = dropdown.getSelectedItem().toString();
        grammaticalChoice = grammaticalChoice.replace(" like","");
        if (!sound.getText().toString().isEmpty()) {
            if (!description.getText().toString().isEmpty()) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"maddogad45@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Sound Illustration Submission");
                i.putExtra(Intent.EXTRA_TEXT   , "Sound: " + sound.getText().toString() +
                        "\nDescription: " + description.getText().toString() +
                        "\nGrammaticalChoice: " + grammaticalChoice);
                try {
                    uri = Uri.fromFile(new File(picturePath));
                } catch (NullPointerException e) {
                    uri = Uri.fromFile(new File(""));
                }
                i.putExtra(Intent.EXTRA_STREAM, uri);
                try {

                    startActivity(Intent.createChooser(i, "Send mail..."));
                    Toast.makeText(getBaseContext(), "Don't change anything, just send", Toast.LENGTH_SHORT).show();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getBaseContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
            else {Toast.makeText(getBaseContext(), "Enter description", Toast.LENGTH_SHORT).show();}
        }
        else {Toast.makeText(getBaseContext(), "Enter sound", Toast.LENGTH_SHORT).show();}
    }

    public void uploadImageButtonClickHandler(View view) {

        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.uploadedImageView);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }



    }


    public void backButtonClickHandler(View view) {
        finish();
    }
}
